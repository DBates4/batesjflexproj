/*
 *        JFlexProject      Name: Dan Bates
 *
 *   Write the specification for a scanner that can scan the tokens
 *   of integer expressions like '32 - 47 * 18'
 */
 
 
/* User Comments and Code */
 

 
 
 
%%

/* Directives and Macros */

%standalone
%public
%class ExpressionScanner
%function next
%type  String
%eofval{
 System.out.println("Gosh, I hit the end of the file.");
 return null;
%eofval}

%{
  /* This code will be inside the class. */
  private int howFarAmI;
%}
  
  
  
other = .
digit = [0-9]
number = {digit}+
operator = [\*\+\-\/]
whitespace = [ \t\n\r]
word = [a-zA-Z]+



%%

/* Lexical Rules  */


{number} {
    /* Rule for number */
    System.out.println("Found Number " + yytext());
    return yytext();
}

{operator} {
    /* Rule for operator */
    System.out.println("Found Operator " + yytext());
    return yytext();
}

{word} {
    /* Rule for word. */
    System.out.println("Found Word: " + yytext());
    return yytext();
}

{whitespace} {
    /* Do nothing for white space */
}

{other} {
    /* Rule for other chars */
    System.out.println("Found other: " + yytext());
    // ignore other stuff
}



